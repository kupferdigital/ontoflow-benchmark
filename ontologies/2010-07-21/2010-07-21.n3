@prefix : <http://www.w3.org/2000/01/rdf-schema#> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix irw: <http://www.ontologydesignpatterns.org/ont/web/irw.owl#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix xs: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix cc: <http://creativecommons.org/ns#> .

<http://ns.inria.fr/nicetag/2010/07/21/voc> a owl:Ontology ;
	owl:versionInfo "Version 0.4" ;
	dc:title "Nice Tag Ontology"@en ;
	dc:description "NiceTag Ontology is an ontology which describes as generally as possible tags or rather tag actions understood as a speech acts occurring on the Web"@en ;
	dc:title "L'Ontologie Nice Tag"@fr ;
	dc:description "NiceTag est une ontologie décrivant le plus généralement possible les tags ou plutôt les actes de taguer comme des actes de langage survenant sur le Web"@fr ;
	dc:title "L'ontologia Nice Tag"@it ;
	dc:description "NiceTag è un'ontologia che descrive nel modo più generale possibile le tag, o meglio le azioni di tagging, come atti linguistici nel Web"@it ;
	dc:title "La ontología Nice Tag"@es ;
	dc:description "NiceTag es una ontología que describe de la manera mas general posible las etiquetas, o mejor las acciones de etiquetado, como actos de habla en la Web"@es ;
	dc:type <http://purl.org/dc/dcmitype/Text> ;
	cc:attributionURL <http://ns.inria.fr/nicetag/> ;
	cc:licence <http://creativecommons.org/licenses/by-sa/3.0/> .

<http://ns.inria.fr/nicetag/2010/07/21/voc#AnnotatedResource> a owl:Class ;
	:label "Annotated resource"@en , "Ressource annotée"@fr , "Risorsa annotata"@it , "Recurso anotado"@es , "Bron met aantekeningen"@nl ;
	:comment "Used to represent the HTTP-accessible realization of an information resource on the Web. The equivalent of irw:WebRepresentation, the Annotated Resource is that which generally triggers the act of tagging. Being dereferenceable by definition, it also provides the address that will be bookmarked on delicious-like sites and work like an anchor for tags. This said, the resource being tagged is in no way limited to HTTP-accessible data (indeed, what supports an act of tagging may very well be what the Web representation represents, a non-information resource for example, instead of the Web representation itself)."@en , "Utilisé pour représenter la réalisation d'une ressource informationnelle accessible sur le Web via le protocole HTTP. La ressource annotée, consultable en ligne, est celle-là même qui déclenche généralement l'acte de taguer. En quoi d'ailleurs elle est l'équivalent de la classe irw:WebRepresentation. Etant par définition déréférençable, elle fournit également l'adresse qui servira d'ancrage aux tags par l'intermédiaire du signet, selon le modèle proposé par delicious. Par contraste, la ressource taguée n'est, quant à elle, aucunement limitée à des données accessibles sur le Web (ce qui est taguée peut bien se révéler ne pas être autre chose que ce que la représentation d'un ressource représente, une ressource non-informationnelle par exemple, plutôt que cette représentation elle-même)."@fr , "Usato per rappresentare la realizzazione di una risorsa informazionale accessibile sul Web mediante il protocollo HTTP. La risorsa annotata è quella che generalmente provaoca l'azione di tagging. Essendo per definizione dereferenziabile, fornisce anche l'indirizzo che sarà salvato su sistemi di bookmarking come delicious, e che avrà funzione di ancoraggio per le tag"@it ;
	owl:equivalentClass irw:WebRepresentation .

<http://ns.inria.fr/nicetag/2010/07/21/voc#TaggedResource> a owl:Class ;
	:label "Tagged resource"@en , "Ressource taguée"@fr , "Risorsa etichettata"@it , "Recurso etiquetado"@es , "Getagde bron"@nl ;
	:comment "Used to represent the resource being tagged. Equivalent of irw:Resource."@en , "Utilisé pour représenter la resource taguée. Equivalent de irw:Resource."@fr , "Usato per rappresentare la risorsa taggata. Equivalente di irw:Resource."@it , "Usado para representar el recursoetiquetado. Equivalente a irw:Resource."@es ;
	owl:equivalentClass irw:Resource .

<http://ns.inria.fr/nicetag/2010/07/21/voc#isRelatedTo> a rdf:Property ;
	:label "Is related to"@en , "Est relié à"@fr , "È connesso con"@it , "Está relacionado con"@es , "Houdt verband met"@nl ;
	:domain <http://ns.inria.fr/nicetag/2010/07/21/voc#TaggedResource> ;
	:comment "Used to link a resource to a sign, which can itself, be of any type, provided its representation is HTTP accessible from a URI. NiceTag defines tags by means of a mere property linking two Resources; this said, the many distinctions found in IRW ontology give a more precise account of nt:TaggedResource (see in particular irw:Resource)."@en , "Utilisé pour lier une ressource à un signe qui, lui-même, peut être de n'importe quel type pourvu que sa représentation, identifiée par une URI, soit accessible via le protocole HTPP. NiceTag définit les tags au moyen d'une propriété qui relie deux Resource; toutefois, l'ontologie IRW permet de comprendre plus précisément la signification de nt:TaggedResource (en particulier la classe irw:Resource)."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#PartOfWebRepresentation> a owl:Class ;
	:subClassOf <http://ontologydesignpatterns.org/ont/web/irw.owl#WebRepresentation> ;
	:label "Part Of Web representation"@en , "Partie de la représentation d'une ressource sur le Web"@fr , "Parte della rappresentazione di una risorsa Web"@it , "Parte de la representación de un recurso en la Web"@es , "Onderdeel van de representatie van een bron op het web"@nl ;
	:comment "Used to represent the part of the Web representation resource being tagged after the \"Point\" TagAction is used."@en , "Utilisé pour représenter la partie de la représentation d'une ressource qui est taguée lorsque qu'il est fait usage de l'acte de taguer \"Pointer\"."@fr , "Usato per rappresentare la parte della rappresentazione di una risorsa Web quando è stato usato l'atto di tagging \"Point\"."@it , "Usado para representar to represent the part of the Web representation resource being tagged after the \"Point\" TagAction is used."@es .

<http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> a owl:Class ;
	:subClassOf <http://www.w3.org/2004/03/trix/rdfg-1/Graph> , <http://rdfs.org/sioc/ns#Item> ;
	owl:unionOf _:node1720hsepex1 .

_:node1720hsepex1 rdf:first <http://ns.inria.fr/nicetag/2010/07/21/voc#OwnerTagAction> .

<http://ns.inria.fr/nicetag/2010/07/21/voc#OwnerTagAction> a owl:Class .

_:node1720hsepex1 rdf:rest _:node1720hsepex2 .

_:node1720hsepex2 rdf:first <http://ns.inria.fr/nicetag/2010/07/21/voc#VisitorTagAction> .

<http://ns.inria.fr/nicetag/2010/07/21/voc#VisitorTagAction> a owl:Class .

_:node1720hsepex2 rdf:rest rdf:nil .

<http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> :label "Tag action"@en , "Acte de taguer"@fr , "Azione di tagging"@it , "Acción de etiquetado"@es , "Taghandeling"@nl ;
	:comment "A super class to describe every tag action as modelled by a named graph according to RDF Graph model (RDFG)."@en , "Une super-classe qui permet de décrire et de modéliser les actes de taguer à l'aide d'un graphe nommé et au moyen du modèle de graphe RDF (RDFG)."@fr , "Una super-classe che permette di descrivere e di modellare ogni azione di tagging con un named graph secondo il modello del grafo RDF (RDFG)."@it , "Una super-clase que permite describir y modelar cada acción de etiquetado utilizando un named graph segun el modelo del grafo RDF (RDFG)."@es .

<http://ns.inria.fr/nicetag/2010/07/21/voc#OwnerTagAction> :subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> ;
	:label "Owner tag action"@en , "Acte de taguer d'un propriétaire"@fr , "Azione di tagging del proprietario"@it , "Acción de etiquetado del proprietario"@es , "Taghandeling eigenaar"@nl ;
	:comment "Used to describe an act of tagging performed by the owner of the URI that identifies the tagged resource."@en , "Utilisé pour décrire un acte de taguer accompli par le propriétaire de l'URI qui identifie la ressource taguée."@fr , "Usato per descrivere un'azione di tagging svolta dal proprietario dell'URI che identifica la risorsa taggata."@it , "Usado para describir una acción de etiquetado realizada por el propietario del URI que identifica el recurso etiquetado."@es .

<http://ns.inria.fr/nicetag/2010/07/21/voc#VisitorTagAction> :subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> ;
	owl:disjointWith <http://ns.inria.fr/nicetag/2010/07/21/voc#OwnerTagAction> ;
	:label "Visitor tag action"@en , "Acte de taguer d'un visiteur"@fr , "Azione di tagging di un visitatore"@it , "Acción de etiquetado de un visitante"@es , "Taghandeling bezoeker"@nl ;
	:comment "Used to describe an act of tagging performed by a the person who browsed a Web representation."@en , "Utilisé pour décrire l'acte de taguer accompli par une personne ayant eu accès à la représentation d'une ressource sur le Web."@fr , "Usato per descrivere un'azione di tagging svolta da una persona che ha avuto accesso alla rappresentazione di una risorsa Web."@it , "Usado para describir una acción de etiquetado realizada por una persona que ha tenido acceso a la representación de un recurso en la Web."@es .

<http://ns.inria.fr/nicetag/2010/07/21/voc#Assert> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> ;
	:label "Assert"@en , "Asserter"@fr , "Asserire"@it , "Afirmar"@es , "Bevestigen"@nl ;
	:comment "Describes the action that is performed with a tag whenever it is used to assert anything about a resource."@en , "Décrit l'action que l'on accomplit avec un tag dès lors que celui-ci est utilisé pour affirmer quoi que ce soit à propos d'une ressource."@fr , "Descrive l'azione che viene svolta con una tag quando questa è utilizzata per affermare qualcosa a proposito di una risorsa."@it , "Describe el acción realizada con una tag quando esta es usada para afirmar algo sobre un recurso."@es .

<http://ns.inria.fr/nicetag/2010/07/21/voc#ExpressFeelings> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> ;
	:label "Express feelings"@en , "Exprimer un ressenti"@fr , "Esprimere un sentimento"@it , "Expresar un sentimiento"@es , "Gevoelens uitdrukken"@nl ;
	:comment "Describes the action that is performed with a tag whenever it is used to express a feeling, an emotion, etc."@en , "Décrit l'action que l'on accomplit avec un tag dès lors que celui-ci est utilisé pour exprimer un sentiment, une émotion, etc."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#Evaluate> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> ;
	:label "Evaluate"@en , "Evaluer"@fr , "Valutare"@it , "Evaluar"@es , "Beoordelen"@nl ;
	:comment """Describes the action that is performed with a tag whenever it is used to give a notation, a mark of approval and disapproval, or,
  more generally speaking, an evaluation."""@en , "Décrit l'action que l'on accomplit avec un tag dès lors que celui-ci est utilisé pour donner une note, marquer son approbation ou sa désapprobation ou, plus généralement, produirez une évaluation."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#GiveAccessRights> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> ;
	:label "Give access rights to"@en , "Accorder des droits d'accès à"@fr , "Accordare diritti di accesso a"@it , "Acordar derechos de acceso a"@es , "Toegangsrechten verlenen"@nl ;
	:comment "Describes the action that is performed with a tag whenever it is used to define to whom access rights to a resource are granted or denied."@en , "Décrit l'action que l'on accomplit avec un tag dès lors que celui-ci est utilisé pour définir les personnes auxquelles sont octroyés des droits d'accès à des ressource en ligne (permission ou refus)."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#Point> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> ;
	:label "Point"@en , "Pointer"@fr , "Puntare"@it , "Puntar"@es , "Aanwijzen"@nl ;
	:comment """Describes the action that is performed with a tag whenever it is used to point to a specific part of a Web representation (the segment of a video,
  a user-generated commentary to a newspaper article, etc.)."""@en , """Décrit l'action que l'on accomplit avec un tag dès lors que celui-ci est utilisé pour faire référence à une portion spécifique de la représentation 
 d'une ressource sur le Web (le segment d'une vidéo, un commentaire produit par les utilisateurs au pied d'un article, etc.)."""@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#SetTask> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#VisitorTagAction> ;
	:label "Set task"@en , "Définir une tâche"@fr , "Definire un compito"@it , "Definir una tarea"@es , "Taak definiëren"@nl ;
	:comment "Describes the action that is performed with a \"todo\" tag whenever it is used to create a task awaiting performance."@en , "Décrit l'action que l'on accomplit avec un tag de type \"àfaire\" dès lors que celui-ci est utilisé pour définir une tâche attendant sa réalisation."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#Share> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> ;
	:label "Share"@en , "Partager"@fr , "Condividere"@it , "Compartir"@es , "Delen"@nl ;
	:comment "Describes the action that is performed with a tag whenever it is used to share the representation of a WebResource on various services - Twitter or Delicious for instance - with the owner of a sioc:UserAccount (not necessarily a foaf:Person as it might be either a bot, a person or an institution whose representatives may well vary over time)."@en , "Décrit l'action que l'on accomplit avec un tag dès lors que celui-ci est utilisé pour partager une représentation d'une ressource sur le Web, notamment sur Delicious ou Twitter. Ce partage ne se fait pas nécessairement avec une autre personne mais plutôt avec le  titulaire d'un compte sur l'un ou l'autre de ces services (sioc:UserAccount, qui n'est pas nécessairement une foaf:Person étant donné qu'il peut s'agir aussi bien d'un robot, d'une personne ou encore d'une institution dont les représentants sont potentiellement amenés à varier au fil du temps)."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#Aggregate> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> ;
	:label "Aggregate"@en , "Agréger"@fr , "Aggregare"@it , "Agregar"@es , "Verzamelen"@nl ;
	:comment "Describes the action that is performed whenever resources are aggregated with a collectively defined tag."@en , "Décrit l'action que l'on accomplit lorsque des ressources sont aggrégées autour d'un tag défini de manière collective."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#Ask> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> ;
	:label "Ask"@en , "Poser une question"@fr , "Porre una domanda"@it , "Preguntar"@es , "Vraag stellen"@nl ;
	:comment "Describes the action that is performed with a tag by asking a question."@en , "Décrit l'action que l'on accomplit avec un tag dès lors que celui-ci est utilisé pour poser une question."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#ManualTagAction> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> ;
	:label "Manual tag action"@en , "Acte de taguer manuel"@fr , "Azione di tagging manuale"@it , "Acción de etiquetado manual"@es , "Handmatige taghandeling"@nl ;
	:comment "Describes tags as manually associated to a resource by a human."@en , "Décrit des tags associés manuellement à une ressource par un être humain."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#AutoTagAction> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> ;
	:label "Auto tag action"@en , "Acte de taguer automatique"@fr , "Azione di tagging automatica"@it , "Acción de etiquetado automàtica"@es , "Geautomatiseerde taghandeling"@nl ;
	:comment "Describes tags as automatically generated and/or associated to a resource by a computer."@en , "Décrit des tags générés et/ou associés automatiquement à une ressource par une machine."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#WebConceptTagAction> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> ;
	:label "Web concept tag action"@en , "Acte de taguer par concept Web"@fr , "Azione di tagging con Web concept"@it , "Acción de etiquetado por Web concept"@es , "Taghandeling met webconcept"@nl ;
	:comment "Describes tagging involving Web concepts (such as geonames)."@en , "Utilisé pour décrire un taggage effectué à l'aide de concepts Web (tels que geonames)."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#SyntacticTagAction> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> ;
	:label "Syntactic Tag Action"@en , "Acte de taguer syntaxique"@fr , "Azione di tagging sintattica"@it , "Acción de etiquetado sintáctica"@es , "Syntactische taghandeling"@nl ;
	:comment "Describes tags whose labels are following a given syntax for improved precision and tractability."@en , "Décrit des tags dont les libellés mobilisent une certaine syntaxe pour accroître leur précision et leur capacité à être manipulés."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#MachineTagAction> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#SyntacticTagAction> ;
	:label "Machine tag action"@en , "Acte de taguer avec machine tag"@fr , "Azione di tagging con machine tag"@it , "Acción de etiquetado con machine tag"@es , "Taghandeling met machinetag"@nl ;
	:comment "Describes tags whose labels are using the syntax of machine tags as implemented first in Flickr.com."@en , "Décrit des tags dont les libellés mobilisent la syntaxe des machine tags telle qu'implémentée d'abord sur le site Flickr.com."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#N-TupleTagAction> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#SyntacticTagAction> ;
	:label "N-tuple tag action"@en , "Acte de taguer avec un tag à n-dimensions"@fr , "Azione di tagging con una tag n-dimensionale"@it , "Accción de etiquetado con una tag n-dimensional"@es , "Taghandeling met n-tuple"@nl ;
	:comment "Describes tagging involving N-tuple tags (double tags, triple tags, etc.)."@en , "Utilisé pour décrire des actes de taguer impliquant des tags dont les libellés ont n-dimensions (double tags, triple tags, etc.)."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#DisambiguatedTagAction> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> ;
	:label "Disambiguate Tag Action"@en , "Acte de taguer avec désambigüisation"@fr , "Azione di tagging con disambiguazione"@it , "Acción de etiquetado con desambiguación"@en , "Taghandeling met gedisambigueerd teken"@nl ;
	:comment "Used when the sign used to tag is disambiguated."@en , "Utilisé lorsque le signe utilisé pour taguer est désambiguïsé."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#N-TupleTagActionMTS> a owl:Class ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#N-TupleTagAction> , <http://ns.inria.fr/nicetag/2010/07/21/voc#MachineTagAction> ;
	:label "N-tuple tag action with machine tag syntax"@en , "Acte de taguer avec un tag à n-dimensions et syntaxe de machine tags"@fr , "Azione di tagging con una tag a n-dimensionale e sintassi delle machine tag"@it , "Acción de etiquetado con una tag n-dimensional y sintaxis de las machine tags"@es , "Taghandeling met een n-tuple met machinetag syntaxis"@nl ;
	:comment "Describes tagging involving N-tuple tags which follow machine tags typed syntax in acontext where Flickr-like APIs are lacking."@en , "Utilisé pour décrire un acte de taguer impliquant des tags dont les libellés ont n-dimensions et suivent la syntaxe des machine tags mais dans un contexte où des APIs semblables à celle de Flickr font défaut."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#TagsCollection> a owl:Class ;
	:label "Tags collection"@en , "Collection de tags"@fr , "Collezione di tag"@it , "Colección de tags"@es , "Tagverzameling"@nl ;
	:comment "Undefined collection of tags."@en , "Une collection de tags ne répondant à aucune définition particulière."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#CommunityTagCollection> a owl:Class ;
	:label "Community tag collection"@en , "Collection de tags communautaire"@fr , "Collezione di tag comunitaria"@it , "Colección de tags comunitaria"@es , "Gemeenschapstagverzameling"@nl ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagsCollection> ;
	:comment "A collection of tags generated by the users of a given online community or Web service."@en , "Une collection de tag constituée par les utilisateurs d'une communauté ou d'un service donnés."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#PersonalTagCollection> a owl:Class ;
	:label "Personal tag collection"@en , "Collection de tags personnelle"@fr , "Collezione di tag personale"@it , "Colección de tags personal"@es , "Persoonlijke tagverzameling"@nl ;
	:subClassOf <http://ns.inria.fr/nicetag/2010/07/21/voc#TagsCollection> ;
	:comment "A colection of tags generated by a single user."@en , "Une collection de tags constituée par un utilisateur précis."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#belongToTagCollection> a rdf:Property ;
	:label "Belongs to tag collection"@en , "Appartient à une collection de tags"@fr , "Appartiene a collezione di tag"@it , "Pertenece a collección de tags"@es , "Behoort toe aan tagverzameling"@nl ;
	:domain <http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> ;
	:range <http://ns.inria.fr/nicetag/2010/07/21/voc#TagCollection> ;
	:comment "Property used to link a single and well individuated TagAction to a TagCollection to account for the various levels of aggregation provided by common tagging applications. There exist different ways to aggregate tags (for instance, http://delicious.com/fabion_gandon/OWL gives a Web representation of all taggings of user \"fabien_gandon\" involving the tag labeled 'OWL' on delicious.com at time \"t\")."@en , "Propriété employée pour associer un acte de taguer singulier à une collection de tags. Ceci sert à rendre compte des multiples manières d'aggréger les tags offertes par les différents services existants (l'URI http://delicious.com/fabion_gandon/OWL permet par exemple d'accéder à une représentation de tous les actes de taguer accomplis par le titulaire du compte \"fabien_gandon\" où figure le libellé \"OWL\" sur delicious.com à un instant \"t\")."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#hasCommunitySign> a rdf:Property ;
	:subPropertyOf <http://ns.inria.fr/nicetag/2010/07/21/voc#isRelatedTo> ;
	:label "Has community sign"@en , "A pour signe collectif"@fr , "Ha segno collettivo"@it , "Tiene signo colectivo"@es , "Heeft een gemeenschappelijk teken"@nl ;
	:comment "Corresponds to uses of collectively approved labels shared inside a community."@en , "Correspond à des usages collectivement constitués de libellés choisis par une communauté."@fr , "Corrisponde all'uso di etichette scelte da una comunità."@it , "Corresponde a uso de etiquetas eligidas por una comunidad."@es .

<http://ns.inria.fr/nicetag/2010/07/21/voc#hasPersonalSign> a rdf:Property ;
	:subPropertyOf <http://ns.inria.fr/nicetag/2010/07/21/voc#isRelatedTo> ;
	:label "Has personal sign"@en , "A pour signe personnel"@fr , "Ha segno personale"@it , "Tiene signo personal"@es , "Heeft een persoonlijk teken"@nl ;
	:comment "Corresponds to uses of user-defined labels that can either serve to gather, categorize, distinguish resources or simply add structure to one's collection of tags."@en , "Correspond à l'emploi de libellés définis par un utilisateur pour rassembler, catégoriser ou distinguer des ressources voire simplement structurer sa propre collection de tags."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#makesMeFeel> a rdf:Property ;
	:subPropertyOf <http://ns.inria.fr/nicetag/2010/07/21/voc#isRelatedTo> ;
	:label "Makes me feel"@en , "Produit sur moi"@fr , "Mi suscita"@it , "Me provoca (me desperta un sentimiento de..)"@es , "Maakt dat ik me voel"@nl ;
	:comment "Property used to indicate that the label of a tag denotes/expresses an emotional reaction."@en , "Propriété utilisée pour indiquer que le libellé d'un tag dénote/exprime une réaction émotionnelle."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#isWorth> a rdf:Property ;
	:subPropertyOf <http://ns.inria.fr/nicetag/2010/07/21/voc#isRelatedTo> ;
	:label "Is worth"@en , "Vaut"@fr , "Ha valore"@it , "Tiene valor"@es , "Is waaard"@nl ;
	:comment "Property used whenever a resource is evaluated, ranked, etc. thanks to a label (\"***\", \"5/10\", \"0\", \"best\", etc.)."@en , "Propriété utilisée pour indiquer qu'une ressource est évaluée, classée, etc. à l'aide d'un libellé (\"***\", \"5/10\", \"0\", \"le meilleur\", etc.)."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#isAbout> a rdf:Property ;
	:subPropertyOf <http://ns.inria.fr/nicetag/2010/07/21/voc#isRelatedTo> ;
	:label "Is about"@en , "A pour thème"@fr , "Ha come tema"@it , "Tiene como tema"@es , "Betreft"@nl ;
	:comment "Property used when the label of a tag describes the topic of a resource."@en , "Propriété utilisé pour indiquer que le libellé d'un tag décrit le thème d'une ressource."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#hasForMedium> a rdf:Property ;
	:subPropertyOf <http://ns.inria.fr/nicetag/2010/07/21/voc#isRelatedTo> ;
	:label "Has for medium"@en , "Est un medium de type"@fr , "È un medium di tipo"@it , "Es un medium de tipo"@es , "Is het medium"@nl ;
	:domain <http://ontologydesignpatterns.org/ont/web/irw.owl#WebRepresentation> ;
	:comment "Property used when the label of a tag indicates the medium which an HTTP-accessible Web representation belongs to (\"forum\", \"video\", \"photo\", \"Webpage\", \"Webservice\", etc.)."@en , "Propriété utilisée quand le libellé d'un tag indique de quel média la représentation de la ressource à laquelle on accède ressortit (\"forum\", \"vidéo\", \"photo\", \"page Web\", \"Webservice\", etc.)."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#isRelevant> a rdf:Property ;
	:subPropertyOf <http://ns.inria.fr/nicetag/2010/07/21/voc#isRelatedTo> ;
	:label "Is relevant"@en , "Est pertinent"@fr , "È pertinente"@it , "Es pertinente"@es , "Is relevant"@nl ;
	:comment "Property used to subsume \"isRelevantToSb\" and \"isRelevantToSt\"."@en , "Propriété utilisée pour subsumer \"isRelevantToSb\" et \"isRelevantToSt\"."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#isRelevantToSb> a rdf:Property ;
	:subPropertyOf <http://ns.inria.fr/nicetag/2010/07/21/voc#isRelevantTo> ;
	:label "Is relevant to somebody"@en , "Est pertinent pour quelqu'un"@fr , "È pertinente a qualcuno"@it , "Es pertinente a alguien"@es , "Is relevant voor iemand"@nl ;
	:range foaf:Person ;
	:comment "Subproperty of 'is relevant to' used to link a resource to the person whom it may be relevant to. In other words, in such a way that no immediate formal link can be inferred between the two except from the point of view of a precise person in addition to the user who defined the said link."@en , "Sous-propriété de 'isRelevantTo', utilisée pour associer une ressource à une personne de telle manière qu'aucun lien formel ne puisse être inféré entre les deux excepté du point de vue du destinataire et du destinateur du tag."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#isRelevantToSt> a rdf:Property ;
	:subPropertyOf <http://ns.inria.fr/nicetag/2010/07/21/voc#isRelevantTo> ;
	:label "Is relevant to something"@en , "Est pertinent par rapport à quelque chose"@fr , "È pertinente a qualcosa"@it , "Es pertinente a algo"@es , "Is relevant met betrekking tot"@nl ;
	:comment "Subproperty of 'is relevant to', used to link a resource to anything that it may be relevant to. In other words, in such a way that no immediate formal link can be inferrend between the two except from the point of view of the creator of the said link."@en , "Sous-propriété de \"isRelevantTo', utilisée pour lier quoi que ce soit à une ressource de telle manière qu'aucun lien immédiat formel ne puisse être inféré entre les deux excepté du point de vue du créateur d'un tel lien."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#elicitsAction> a rdf:Property ;
	:subPropertyOf <http://ns.inria.fr/nicetag/2010/07/21/voc#isRelatedTo> ;
	:label "Elicits action"@en , "Suscite l'action"@fr , "Suscita l'azione"@it , "Provoca el acción"@es , "Ontlokt de handeling"@nl ;
	:comment "Property used when a resource elicits an action to be performed."@en , "Propriété utilisée quand une ressource suscite l'accomplissement d'une action."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#sentTo> a rdf:Property ;
	:subPropertyOf <http://ns.inria.fr/nicetag/2010/07/21/voc#isRelatedTo> ;
	:label "Sent to"@en , "Envoyé à"@fr , "Inviato a"@it , "Enviado a"@es , "Verstuurd aan"@nl ;
	:domain <http://ontologydesignpatterns.org/ont/web/irw.owl#WebRepresentation> ;
	:range <http://rdfs.org/sioc/ns#UserAccount> ;
	:comment "Property used when the label of a tag indicates to whom a resource was suggested (models existing features like delicious \"for:-username\" tags)."@en , "Propriété utilisée quand le libellé d'un tag indique à quel utilisateur une ressource a été suggérée (modélise des fonctionalités existantes telles que les tags \"for:-username\" sur Delicious)."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#sentBy> a rdf:Property ;
	:subPropertyOf <http://ns.inria.fr/nicetag/2010/07/21/voc#isRelatedTo> ;
	:label "Sent by"@en , "Envoyé par"@fr , "Inviato da"@it , "Enviado por"@es , "Verstuurd door"@nl ;
	:domain <http://ontologydesignpatterns.org/ont/web/irw.owl#WebRepresentation> ;
	:range <http://rdfs.org/sioc/ns#UserAccount> ;
	:comment "Corresponds to uses of labels that indicate by whom a resource was suggested."@en , "Correspond à l'utilisation de signes qui indiquent à qui une ressource a été suggérée."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#canBeReadBy> a rdf:Property ;
	:subPropertyOf <http://ns.inria.fr/nicetag/2010/07/21/voc#isRelatedTo> ;
	:label "Can be read by"@en , "Peut être lu par"@fr , "Può essere letto da"@it , "Puede ser leído por"@es , "Kan gelezen worden door"@nl ;
	:domain <http://ontologydesignpatterns.org/ont/web/irw.owl#WebRepresentation> ;
	:range foaf:Person ;
	:comment "Property used when the label of a tag indicates to whom access rights to the tagged resource are conferred."@en , "Corresponds à l'utilisation de tags dont le libellé indique le destinataire auquel des droits d'accès définis sur une ressource donnée ont été octroyés."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#cannotBeReadBy> a rdf:Property ;
	:subPropertyOf <http://ns.inria.fr/nicetag/2010/07/21/voc#isRelatedTo> ;
	:label "Cannot Be Read By"@en , "Ne peut être lu par"@fr , "Non può essere letto da"@it , "No puede ser leído por"@es , "Kan niet gelezen worden door"@nl ;
	:domain <http://ontologydesignpatterns.org/ont/web/irw.owl#WebRepresentation> ;
	:range foaf:Person ;
	:comment "Property used when the label of a tag indicates to whom access right to the tagged resource are denied."@en , "Propriété utilisée quand le libellé d'un tag indique le destinataire auquel des droits d'accès définis sur une ressource donnée ont été déniés."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#hasPart> a rdf:Property ;
	:subPropertyOf <http://ns.inria.fr/nicetag/2010/07/21/voc#isRelatedTo> ;
	:label "Has part"@en , "A pour partie"@fr , "Ha come parte"@it , "Tiene como parte"@es , "Heeft als deel"@nl ;
	:domain <http://ontologydesignpatterns.org/ont/web/irw.owl#WebRepresentation> ;
	:range <http://ns.inria.fr/nicetag/2010/07/21/voc#PartOfWebRepresentation> ;
	:comment "Property used when the label of a tag indicates which part of a Web representation is being tagged."@en , "Propriété utilisée quand le libellé d'un tag indique quelle partie de la représentation d'une ressource sur le Web est taguée."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#raisesQuestionAbout> a rdf:Property ;
	:subPropertyOf <http://ns.inria.fr/nicetag/2010/07/21/voc#isRelatedTo> ;
	:label "Raises question about"@en , "Soulève une interrogation au sujet de"@fr , "Solleva una domanda su"@it , "Plantea una cuestión sobre"@es , "Roept vragen op over"@nl ;
	:comment "Property used when the label of a tag indicates that a question is being asked."@en , "Propriété utilisée quand le libellé d'un tag indique qu'une question est posée."@fr .

<http://ns.inria.fr/nicetag/2010/07/21/voc#TagAction> owl:unionOf _:node1720hsepex3 .

_:node1720hsepex3 rdf:first <http://ns.inria.fr/nicetag/2010/07/21/voc#OwnerTagAction> ;
	rdf:rest _:node1720hsepex4 .

_:node1720hsepex4 rdf:first <http://ns.inria.fr/nicetag/2010/07/21/voc#VisitorTagAction> ;
	rdf:rest rdf:nil .
