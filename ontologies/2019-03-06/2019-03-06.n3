@prefix :      <https://w3id.org/ep-plan#> .
@prefix owl:   <http://www.w3.org/2002/07/owl#> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xml:   <http://www.w3.org/XML/1998/namespace> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .

:constrains  a         owl:ObjectProperty ;
        rdfs:comment   "Inverse of ep-plan:hasConstraint." ;
        rdfs:domain    :Constraint ;
        rdfs:label     "constrains" ;
        rdfs:range     [ a            owl:Class ;
                         owl:unionOf  ( :CommunicationSpecification :Plan :Step )
                       ] ;
        owl:inverseOf  :hasConstraint .

<http://www.w3.org/ns/prov#Activity>
        a       owl:Class .

<http://www.w3.org/ns/prov#Communication>
        a       owl:Class .

:correspondsToCommunicationSpecification
        a              owl:FunctionalProperty , owl:ObjectProperty ;
        rdfs:comment   "ep-plan:correspondsToCommunicationSpecification links ep-plan:Communication to its planned specification." ;
        rdfs:domain    :Communication ;
        rdfs:label     "correspondsToCommunicationSpecification" ;
        rdfs:range     :CommunicationSpecification ;
        owl:inverseOf  :hasCorrespondingCommunication .

:isSenderIn  a        owl:ObjectProperty ;
        rdfs:comment  "Inverse of ep-plan:hasSender." ;
        rdfs:domain   :Step ;
        rdfs:label    "isSenderIn" ;
        rdfs:range    :CommunicationSpecification .

:isCommunicationSpecificationOfPlan
        a                   owl:ObjectProperty ;
        rdfs:comment        "ep-plan:isCommunicationSpecificationOfPlan binds a ep-plan:CommunicationSpecification to the ep-plan:Plan it corresponds to." ;
        rdfs:domain         :CommunicationSpecification ;
        rdfs:label          "isCommunicationSpecificationOfPlan" ;
        rdfs:range          :Plan ;
        rdfs:subPropertyOf  :isElementOfPlan .

:hasSender  a          owl:ObjectProperty ;
        rdfs:comment   "ep-plan:hasSender links an ep-plan:Step to  an ep-plan:CommunicationSpecification and indicates that the step is expected to produce an ep-plan:Variable that will be transferred according to the ep-plan:CommunicationSpecification." ;
        rdfs:domain    :CommunicationSpecification ;
        rdfs:label     "hasSender" ;
        rdfs:range     :Step ;
        owl:inverseOf  :isSenderIn .

:hasPayload  a         owl:ObjectProperty ;
        rdfs:comment   "ep-plan:hasPayload links an ep-plan:Variable to an ep-plan:CommunicationSpecification and identyfies the variable that will be transferred according to the ep-plan:CommunicationSpecification." ;
        rdfs:domain    :CommunicationSpecification ;
        rdfs:label     "hasPayload" ;
        rdfs:range     :Variable ;
        owl:inverseOf  :isPayloadIn .

:hasOutputVariable  a  owl:ObjectProperty ;
        rdfs:comment   "ep-plan:hasOutputVariable binds a ep-plan:Step to the ep-plan:Variable that will be produced as output in the planned execution" ;
        rdfs:domain    :Step ;
        rdfs:label     "hasOutputVariable" ;
        rdfs:range     :Variable ;
        owl:inverseOf  :isOutputVariableOf .

:isPermittedAgentFor  a  owl:ObjectProperty ;
        rdfs:comment  "Inverse of ep-plan:hasPermittedAgent." ;
        rdfs:domain   :ResponsibleAgent ;
        rdfs:label    "isPermittedAgentFor" ;
        rdfs:range    [ a            owl:Class ;
                        owl:unionOf  ( :Plan :Step )
                      ] .

:hasRecipient  a       owl:ObjectProperty ;
        rdfs:comment   "ep-plan:hasRecipient links an ep-plan:Step to an ep-plan:CommunicationSpecification and indicates that the step is expected to receive an ep-plan:Variable that will be transferred according to the ep-plan:CommunicationSpecification." ;
        rdfs:domain    :CommunicationSpecification ;
        rdfs:label     "hasRecipient" ;
        rdfs:range     :Step ;
        owl:inverseOf  :isRecipientIn .

:isStepOfPlan  a            owl:ObjectProperty ;
        rdfs:comment        "ep-plan:isStepOfPlan binds a ep-plan:Step to the ep-plan:Plan it corresponds to." ;
        rdfs:domain         :Step ;
        rdfs:label          "isStepOfPlan" ;
        rdfs:range          :Plan ;
        rdfs:subPropertyOf  :isElementOfPlan .

<http://purl.org/net/p-plan#Variable>
        a                    owl:Class ;
        owl:equivalentClass  :Variable .

:evaluatedFor  a      owl:ObjectProperty ;
        rdfs:comment  "Inverse of ep-plan:evaluatedAgainst." ;
        rdfs:domain   :Constraint ;
        rdfs:label    "evaluatedFor" ;
        rdfs:range    [ a            owl:Class ;
                        owl:unionOf  ( :Activity :Communication )
                      ] .

<http://purl.org/net/p-plan#Plan>
        a                    owl:Class ;
        owl:equivalentClass  :Plan .

:hasConstraintImplementation
        a              owl:ObjectProperty ;
        rdfs:comment   "ep-plan:hasConstraintImplementation links ep-plan:Constraint to any resource that provides details on how such constraint is implemented (e.g. a rule, script, etc.)." ;
        rdfs:domain    :Constraint ;
        rdfs:label     "hasConstraintImplementation" ;
        rdfs:range     rdfs:Resource ;
        owl:inverseOf  :isConstraintImplementationOf .

:isPrecededBy  a            owl:TransitiveProperty , owl:ObjectProperty ;
        rdfs:comment        "Property that asserts which Step preceeds the current one." ;
        rdfs:domain         :Step ;
        rdfs:label          "isPrecededBy" ;
        rdfs:range          :Step ;
        rdfs:subPropertyOf  owl:topObjectProperty ;
        owl:inverseOf       :precedes .

:isOutputVariableOf  a  owl:ObjectProperty ;
        rdfs:comment  "Inverse of p-plan:hasOutputVariable." ;
        rdfs:domain   :Variable ;
        rdfs:label    "isOutputVariableOf" ;
        rdfs:range    :Step .

:MultiActivity  a        owl:Class ;
        rdfs:comment     "An activity that typically corresponds to a MultiStep which is decomposed as a Plan or the main execution activity that executed the plan. ep-plan:MultiActivity typically generates ep-plan:ExecutionTraceBundle." ;
        rdfs:label       "Multi Activity" ;
        rdfs:subClassOf  :Activity .

:qualifiedEvaluation  a  owl:ObjectProperty ;
        rdfs:comment  "ep-plan:qualifiedEvaluation links ep-plan:Constraint to ep-plan:ConstriantEvaluation." ;
        rdfs:domain   :Constraint ;
        rdfs:label    "qualifiedEvaluation" ;
        rdfs:range    :ConstraintEvaluation .

:isVariableOfPlan  a        owl:ObjectProperty ;
        rdfs:comment        "ep-plan:isVariableOfPlan binds a ep-plan:Variable to the ep-plan:Plan it corresponds to." ;
        rdfs:domain         :Variable ;
        rdfs:label          "isVariableOfPlan" ;
        rdfs:range          :Plan ;
        rdfs:subPropertyOf  :isElementOfPlan .

:correspondsToVariable
        a              owl:FunctionalProperty , owl:ObjectProperty ;
        rdfs:comment   "ep-plan:correspondsToVariable binds a ep-plan:Entity (used by a ep-plan:Activity in the execution of a plan) to the ep-plan:Variable it represented it in the ep-plan:Plan." ;
        rdfs:domain    :Entity ;
        rdfs:label     "correspondsToVariable" ;
        rdfs:range     :Variable ;
        owl:inverseOf  :hasCorrespondingEntity .

<http://purl.org/net/p-plan#hasOutputVar>
        a                       owl:ObjectProperty ;
        owl:equivalentProperty  :hasOutputVariable .

<http://purl.org/dc/terms/license>
        a       owl:AnnotationProperty .

<http://www.w3.org/ns/prov#Agent>
        a       owl:Class .

:FailedActivity  a       owl:Class ;
        rdfs:comment     "A concept representing an activity that was not executed in full, for example, due to constraint violation. Such an activity usually marks the point when the plan execution was interrupted due to some problem." ;
        rdfs:label       "Failed Activity" ;
        rdfs:subClassOf  :Activity .

:includesStep  a            owl:ObjectProperty ;
        rdfs:comment        "Inverse of ep-plan:isStepOfPlan." ;
        rdfs:domain         :Plan ;
        rdfs:label          "includesStep" ;
        rdfs:range          :Step ;
        rdfs:subPropertyOf  :includesPlanElement ;
        owl:inverseOf       :isStepOfPlan .

:isPayloadIn  a       owl:ObjectProperty ;
        rdfs:comment  "Inverse of ep-plan:hasPayload." ;
        rdfs:domain   :Variable ;
        rdfs:label    "isPayloadIn" ;
        rdfs:range    :CommunicationSpecification .

:satisfied  a               owl:ObjectProperty ;
        rdfs:comment        "ep-plan:satisfied links ep-plan:Activity or ep-plan:Communication to a ep-plan:Constraint. The property indicates that the execution trace element satisfied the constraint." ;
        rdfs:domain         [ a            owl:Class ;
                              owl:unionOf  ( :Activity :Communication )
                            ] ;
        rdfs:label          "satisfied" ;
        rdfs:range          :Constraint ;
        rdfs:subPropertyOf  :evaluatedAgainst ;
        owl:inverseOf       :satisfiedBy .

:hasPermittedAgent  a  owl:ObjectProperty ;
        rdfs:comment   "ep-plan:hasPermittedAgent links ep-plan:Step or the overall ep-plan:Plan to an ep-plan:ResponsibleAgent denoting that  the agent is allowed to assume some form of responsibility during the execution. Please note that not all linked ep-plan:ResponsibleAgent(s) must also appear in the execution trace as ep-plan:Agent. ep-plan:hasPermittedAgent relationship merely indicates that it is possible for this agent to asssume some responsibility during the execution." ;
        rdfs:domain    [ a            owl:Class ;
                         owl:unionOf  ( :Plan :Step )
                       ] ;
        rdfs:label     "hasPermittedAgent" ;
        rdfs:range     :ResponsibleAgent ;
        owl:inverseOf  :isPermittedAgentFor .

<http://purl.org/net/p-plan#Step>
        a                    owl:Class ;
        owl:equivalentClass  :Step .

:Constraint  a            owl:Class ;
        rdfs:comment      "A restriction placed on a particular step or a plan that is evaluated against an execution trace." ;
        rdfs:label        "Constraint" ;
        owl:disjointWith  [ a            owl:Class ;
                            owl:unionOf  ( :CommunicationSpecification :Policy :ResponsibleAgent :Step )
                          ] .

:ResponsibleAgent  a      owl:Class ;
        rdfs:comment      "An agent that can bear some responsibility for execution of activities corresponding to the planned steps." ;
        rdfs:label        "ResponsibleAgent" ;
        owl:disjointWith  [ a            owl:Class ;
                            owl:unionOf  ( :CommunicationSpecification :Constraint :Policy :Step )
                          ] .

<http://www.w3.org/ns/prov#Entity>
        a       owl:Class .

:Policy  a                owl:Class ;
        rdfs:comment      "A generic representation of a collection of rules that may be applicable in the context of a specific plan. A policy can provide a rationale for constraints being applied to steps as well as explain reasons including other plan elements. For example, a data protection policy might require that steps are executed by specific agents and that a data anonymisation step is included in the plan." ;
        rdfs:label        "Policy" ;
        owl:disjointWith  [ a            owl:Class ;
                            owl:unionOf  ( :CommunicationSpecification :Constraint :ResponsibleAgent :Step )
                          ] .

<http://purl.org/dc/terms/created>
        a       owl:AnnotationProperty .

<http://www.w3.org/ns/prov#Bundle>
        a       owl:Class .

rdfs:Resource  a  owl:Class .

:isConstraintImplementationOf
        a             owl:ObjectProperty ;
        rdfs:comment  "Inverse of ep-plan:hasConstraintImplementation." ;
        rdfs:domain   rdfs:Resource ;
        rdfs:label    "isConstraintImplementationOf" ;
        rdfs:range    :Constraint .

:CommunicationSpecification
        a                 owl:Class ;
        rdfs:comment      "A specification of how communication/exchange of variables between two steps should be performed." ;
        rdfs:label        "CommunicationSpecification" ;
        owl:disjointWith  [ a            owl:Class ;
                            owl:unionOf  ( :Constraint :Policy :ResponsibleAgent :Step )
                          ] .

<http://purl.org/vocab/vann/preferredNamespacePrefix>
        a       owl:AnnotationProperty .

<http://purl.org/net/p-plan#isInputVarOf>
        a                       owl:ObjectProperty ;
        owl:equivalentProperty  :isInputVariableOf .

:includesCommunicationSpecification
        a                   owl:ObjectProperty ;
        rdfs:comment        "Inverse of ep-plan:isCommunicationSpecificationOfPlan." ;
        rdfs:domain         :Plan ;
        rdfs:label          "includesCommunicationSpecification" ;
        rdfs:range          :CommunicationSpecification ;
        rdfs:subPropertyOf  :includesPlanElement ;
        owl:inverseOf       :isCommunicationSpecificationOfPlan .

<http://purl.org/net/p-plan#Entity>
        a                    owl:Class ;
        owl:equivalentClass  :Entity .

:isAchievedBy  a      owl:ObjectProperty ;
        rdfs:comment  "ep-plan:isAchievedBy links ep-plan:Objective to a plan or individual plan element that is expected to achieve the objective specified by ep-plan:Objective." ;
        rdfs:domain   :Objective ;
        rdfs:label    "isAchievedBy" ;
        rdfs:range    [ a            owl:Class ;
                        owl:unionOf  ( :CommunicationSpecification :Constraint :Plan :ResponsibleAgent :Step :Variable )
                      ] .

:isRationaleOf  a     owl:ObjectProperty ;
        rdfs:comment  "Inverse of hasRationale." ;
        rdfs:domain   :Rationale ;
        rdfs:label    "isRationaleOf" ;
        rdfs:range    [ a            owl:Class ;
                        owl:unionOf  ( :CommunicationSpecification :Constraint :Objective :Plan :ResponsibleAgent :Step :Variable )
                      ] .

<http://purl.org/net/p-plan#MultiStep>
        a                    owl:Class ;
        owl:equivalentClass  :MultiStep .

:isConstraintOfPlan  a      owl:ObjectProperty ;
        rdfs:comment        "ep-plan:isConstraintOfPlan binds a ep-plan:Constraint to the ep-plan:Plan it corresponds to." ;
        rdfs:domain         :Constraint ;
        rdfs:label          "isConstraintOfPlan" ;
        rdfs:range          :Plan ;
        rdfs:subPropertyOf  :isElementOfPlan .

<http://purl.org/vocab/vann/preferredNamespaceUri>
        a       owl:AnnotationProperty .

<http://purl.org/net/p-plan#Bundle>
        a                    owl:Class ;
        owl:equivalentClass  :ExecutionTraceBundle .

:violated  a                owl:ObjectProperty ;
        rdfs:comment        "ep-plan:satisfied links ep-plan:Activity or ep-plan:Communication to a ep-plan:Constraint. The property indicates that the execution trace element did not satisfy the constraint." ;
        rdfs:domain         [ a            owl:Class ;
                              owl:unionOf  ( :Activity :Communication )
                            ] ;
        rdfs:label          "violated" ;
        rdfs:range          :Constraint ;
        rdfs:subPropertyOf  :evaluatedAgainst ;
        owl:inverseOf       :violatedBy .

:hasCorrespondingActivity
        a             owl:ObjectProperty ;
        rdfs:comment  "Inverse of ep-plan:correspondsToStep." ;
        rdfs:domain   :Step ;
        rdfs:label    "hasCorrespondingActivity" ;
        rdfs:range    :Activity .

:Entity  a               owl:Class ;
        rdfs:comment     "An element of an execution trace representing an indidual entity corresponding to a  variable of a plan which was used and/or generated by activities recorded in the execution trace." ;
        rdfs:label       "Entity" ;
        rdfs:subClassOf  <http://www.w3.org/ns/prov#Entity> .

:achieves  a           owl:ObjectProperty ;
        rdfs:comment   "Inverse of ep-plan:isAchievedBy." ;
        rdfs:domain    [ a            owl:Class ;
                         owl:unionOf  ( :CommunicationSpecification :Constraint :Plan :ResponsibleAgent :Step :Variable )
                       ] ;
        rdfs:label     "achieves" ;
        rdfs:range     :Objective ;
        owl:inverseOf  :isAchievedBy .

:Variable  a          owl:Class ;
        rdfs:comment  "An ep-plan:Variable represents a description of the input of the planned Activity (ep-plan:Step)" ;
        rdfs:label    "Variable" .

:hasPart  a            owl:ObjectProperty ;
        rdfs:comment   "ep-plan:hasPart links amn ep-plan:MultiVariable to another ep-plan:Variable that represents a smaller part of a composite variable." ;
        rdfs:domain    :MultiVariable ;
        rdfs:label     "hasPart" ;
        rdfs:range     :Variable ;
        owl:inverseOf  :isPartOf .

<http://purl.org/net/p-plan#correspondsToVariable>
        a                       owl:ObjectProperty ;
        owl:equivalentProperty  :correspondsToVariable .

:Plan   a                owl:Class ;
        rdfs:comment     "An ep-plan:Plan is a specific type of prov:Plan. It is composed of smaller steps that use and produce variables." ;
        rdfs:label       "Plan" ;
        rdfs:subClassOf  <http://www.w3.org/ns/prov#Plan> .

<http://purl.org/dc/terms/title>
        a       owl:AnnotationProperty .

:isElementOfPlan  a   owl:ObjectProperty ;
        rdfs:comment  "ep-plan:isElementOfPlan binds a ep-plan:PlanElement to the ep-plan:Plan it corresponds to." ;
        rdfs:domain   [ a            owl:Class ;
                        owl:unionOf  ( :CommunicationSpecification :Constraint :Objective :Plan :ResponsibleAgent :Step :Variable )
                      ] ;
        rdfs:label    "isElementOfPlan" ;
        rdfs:range    :Plan .

:ConstraintEvaluation
        a             owl:Class ;
        rdfs:comment  "A concept used to qualify the relationship between a trace activity and a constraint associated with a step to which such activity corresponds to. The concept provides an opportunity to link additional information documenting the constraint evaluation process (e.g. a tool or an algorithm used to check for constraint violations, time when constraints were evaluated, etc.)." ;
        rdfs:label    "ConstraintEvaluation" .

:hasRationale  a       owl:ObjectProperty ;
        rdfs:comment   "A property linking a plan element to the description of a rationale which provides some information on why such element was included in a plan." ;
        rdfs:domain    [ a            owl:Class ;
                         owl:unionOf  ( :CommunicationSpecification :Constraint :Objective :Plan :ResponsibleAgent :Step :Variable )
                       ] ;
        rdfs:label     "hasRationale" ;
        rdfs:range     :Rationale ;
        owl:inverseOf  :isRationaleOf .

<http://purl.org/net/p-plan#isPrecededBy>
        a                       owl:ObjectProperty ;
        owl:equivalentProperty  :isPrecededBy .

<http://purl.org/net/p-plan#Activity>
        a                    owl:Class ;
        owl:equivalentClass  :Activity .

:includesObjective  a       owl:ObjectProperty ;
        rdfs:comment        "Inverse of ep-plan:isObjectiveOfPlan." ;
        rdfs:domain         :Plan ;
        rdfs:label          "includesObjective" ;
        rdfs:range          :Objective ;
        rdfs:subPropertyOf  :includesPlanElement ;
        owl:inverseOf       :isObjectiveOfPlan .

:MultiStep  a            owl:Class ;
        rdfs:comment     "ep-plan:MultiStep is the representation of a composite step that can be described (decomposed) as another ep-plan:Plan." ;
        rdfs:label       "MultiStep" ;
        rdfs:subClassOf  :Step .

:hasCorrespondingCommunication
        a             owl:ObjectProperty ;
        rdfs:comment  "Inverse of correspondsToCommunicationSpecification" ;
        rdfs:domain   :CommunicationSpecification ;
        rdfs:label    "hasCorrespondingCommunication" ;
        rdfs:range    :Communication .

:includesPolicy  a          owl:ObjectProperty ;
        rdfs:comment        "Inverse of ep-plan:isPolicyOfPlan." ;
        rdfs:domain         :Plan ;
        rdfs:label          "includesPolicy" ;
        rdfs:range          :Policy ;
        rdfs:subPropertyOf  :includesPlanElement ;
        owl:inverseOf       :isPolicyOfPlan .

<http://purl.org/net/p-plan#isOutputVarOf>
        a                       owl:ObjectProperty ;
        owl:equivalentProperty  :isOutputVariableOf .

:MultiVariable  a        owl:Class ;
        rdfs:comment     "ep-plan:MultiVariable represents a composite variable that can be broken down into smaller parts represented as ep-plan:Variable(s)." ;
        rdfs:label       "MultiVariable" ;
        rdfs:subClassOf  :Variable .

:violatedBy  a              owl:ObjectProperty ;
        rdfs:comment        "Inverse of ep-plan:violated." ;
        rdfs:domain         :Constraint ;
        rdfs:label          "violatedBy" ;
        rdfs:range          [ a            owl:Class ;
                              owl:unionOf  ( :Activity :Communication )
                            ] ;
        rdfs:subPropertyOf  :evaluatedFor .

:Step   a                 owl:Class ;
        rdfs:comment      "An ep-plan:Step represents the planned execution activity." ;
        rdfs:label        "Step" ;
        owl:disjointWith  :Variable .

<http://purl.org/dc/terms/creator>
        a       owl:AnnotationProperty .

:isResponsibleAgentOfPlan
        a                   owl:ObjectProperty ;
        rdfs:comment        "ep-plan:isResponsibleAgentOfPlan binds a ep-plan:ResponsibleAgent to the ep-plan:Plan it corresponds to." ;
        rdfs:domain         :ResponsibleAgent ;
        rdfs:label          "isResposibleAgentOfPlan" ;
        rdfs:range          :Plan ;
        rdfs:subPropertyOf  :isElementOfPlan .

:precedes  a          owl:TransitiveProperty , owl:ObjectProperty ;
        rdfs:comment  "Inverse of ep-plan:isPrecededBy." ;
        rdfs:domain   :Step ;
        rdfs:label    "precedes" ;
        rdfs:range    :Step .

:includesConstraint  a      owl:ObjectProperty ;
        rdfs:comment        "Inverse of ep-plan:isConstraintOfPlan." ;
        rdfs:domain         :Plan ;
        rdfs:label          "includesConstraint" ;
        rdfs:range          :Constraint ;
        rdfs:subPropertyOf  :includesPlanElement ;
        owl:inverseOf       :isConstraintOfPlan .

<http://www.w3.org/ns/prov#Plan>
        a       owl:Class .

:evaluatedAgainst  a   owl:ObjectProperty ;
        rdfs:comment   "ep-plan:evaluatedAgainst links ep-plan:Activity or ep-plan:Communication to an ep-plan:Constraint. The use of subproperties of ep-plan:evaluatedAgainst is encouraged to indicate whether the execution trace element satisfied the constraint." ;
        rdfs:domain    [ a            owl:Class ;
                         owl:unionOf  ( :Activity :Communication )
                       ] ;
        rdfs:label     "evaluatedAgainst" ;
        rdfs:range     :Constraint ;
        owl:inverseOf  :evaluatedFor .

:wasEvaluatedtraceElement
        a             owl:ObjectProperty ;
        rdfs:comment  "Inverse of ep-plan:evaluatedTraceElement." ;
        rdfs:domain   [ a            owl:Class ;
                        owl:unionOf  ( :Activity :Communication )
                      ] ;
        rdfs:label    "wasEvaluatedTraceElement" ;
        rdfs:range    :ConstraintEvaluation .

<http://www.w3.org/ns/prov#Collection>
        a       owl:Class .

:isDecomposedAsPlan  a  owl:ObjectProperty ;
        rdfs:comment  "The ep-plan:isDecomposedAsPlan relationship binds a ep-plan:MultiStep to the ep-plan:Plan holding the definition of that step. That is, ep-plan:isDecomposedAsPlan links the MultiStep to the Plan sptecification where it is decomposed." ;
        rdfs:domain   :MultiStep ;
        rdfs:label    "isDecomposedAsPlan" ;
        rdfs:range    :Plan .

:isRecipientIn  a     owl:ObjectProperty ;
        rdfs:comment  "Inverse of ep-plan:hasRecipient." ;
        rdfs:domain   :Step ;
        rdfs:label    "isRecipientIn" ;
        rdfs:range    :CommunicationSpecification .

:isElementOfTrace  a  owl:ObjectProperty ;
        rdfs:comment  "Inverse of ep-plan:hasTraceElement." ;
        rdfs:domain   [ a            owl:Class ;
                        owl:unionOf  ( :Activity :Agent :Communication :Entity )
                      ] ;
        rdfs:label    "isElementOfTrace" ;
        rdfs:range    :ExecutionTraceBundle .

<http://purl.org/net/p-plan#hasInputVar>
        a                       owl:ObjectProperty ;
        owl:equivalentProperty  :hasInputVariable .

:hasInputVariable  a   owl:ObjectProperty ;
        rdfs:comment   "ep-plan:hasInputVariable binds a ep-plan:Step to the ep-plan:Variable that takes as input for the planned execution." ;
        rdfs:domain    :Step ;
        rdfs:label     "hasInputVariable" ;
        rdfs:range     :Variable ;
        owl:inverseOf  :isInputVariableOf .

:hasCorrespondingEntity
        a             owl:ObjectProperty ;
        rdfs:comment  "Inverse of ep-plan:correspondsToVariable." ;
        rdfs:domain   :Variable ;
        rdfs:label    "hasCorrespondingEntity" ;
        rdfs:range    :Entity .

<http://purl.org/dc/terms/description>
        a       owl:AnnotationProperty .

:decomposesMultiStep  a  owl:ObjectProperty ;
        rdfs:comment   "Inverse of ep-plan:isDecomposedAsPlan." ;
        rdfs:domain    :Plan ;
        rdfs:label     "decomposesMultiStep" ;
        rdfs:range     :MultiStep ;
        owl:inverseOf  :isDecomposedAsPlan .

:isObjectiveOfPlan  a       owl:ObjectProperty ;
        rdfs:comment        "ep-plan:isObjectiveOfPlan binds a ep-plan:Objective to the ep-plan:Plan it corresponds to." ;
        rdfs:domain         :Objective ;
        rdfs:label          "isObjectiveOfPlan" ;
        rdfs:range          :Plan ;
        rdfs:subPropertyOf  :isElementOfPlan .

:evaluatedTraceElement
        a              owl:ObjectProperty ;
        rdfs:comment   "ep-plan:evaluatedTraceElement links ep-plan:ConstriantEvaluation to a ep-plan:Activity or ep-plan:Communication that were evaluated against a ep-plan:Constraint defined in the plan." ;
        rdfs:domain    :ConstraintEvaluation ;
        rdfs:label     "evaluatedTraceElement" ;
        rdfs:range     [ a            owl:Class ;
                         owl:unionOf  ( :Activity :Communication )
                       ] ;
        owl:inverseOf  :wasEvaluatedtraceElement .

:isSubPlanOfPlan  a         owl:TransitiveProperty , owl:ObjectProperty ;
        rdfs:comment        "ep-plan:Plan may be a subplan of another bigger ep-plan:Plan. The relation ep-plan:isSubPlanOfPlan is used to state the link among the two different plans. Note that if p1 is a sub-plan of p2, p1 will not necessarily be a step of p2. A multistep will represent p1 in p2, and link to p1 with the hasStepDecomposition relationship." ;
        rdfs:domain         :Plan ;
        rdfs:label          "isSubPlanOfPlan" ;
        rdfs:range          :Plan ;
        rdfs:subPropertyOf  :isElementOfPlan .

<https://w3id.org/ep-plan>
        a                owl:Ontology ;
        rdfs:comment     "PROV extension for linking Plans and parts of plans to their respective executions. " ;
        <http://purl.org/dc/terms/created>
                "2019-03-06"^^xsd:date ;
        <http://purl.org/dc/terms/creator>
                "https://www.abdn.ac.uk/ncs/profiles/milan.markovic/"^^xsd:anyURI , "http://delicias.dia.fi.upm.es/members/DGarijo#me"^^xsd:anyURI ;
        <http://purl.org/dc/terms/description>
                "PROV extension for linking Plans and parts of plans to their respective executions."@en ;
        <http://purl.org/dc/terms/license>
                "http://creativecommons.org/licenses/by-nc-sa/2.0/" ;
        <http://purl.org/dc/terms/title>
                "The EP-Plan ontology"@en ;
        <http://purl.org/vocab/vann/preferredNamespacePrefix>
                "ep-plan" ;
        <http://purl.org/vocab/vann/preferredNamespaceUri>
                "https://w3id.org/ep-plan" ;
        owl:versionInfo  "1.0" .

:includesResponsibleAgent
        a                   owl:ObjectProperty ;
        rdfs:comment        "Inverse of ep-plan:isResponsibleAgentOfPlan." ;
        rdfs:domain         :Plan ;
        rdfs:label          "includesResponsibleAgent" ;
        rdfs:range          :ResponsibleAgent ;
        rdfs:subPropertyOf  :includesPlanElement ;
        owl:inverseOf       :isResponsibleAgentOfPlan .

:Objective  a         owl:Class ;
        rdfs:comment  " ep-plan:Objective describes information detailing a specific purpose of a plan. Plans can have more than one objectives. Users are encouraged to extend this class, for example, to identify primary and secondary objectives of a plan." ;
        rdfs:label    "Objective" .

:ExecutionTraceBundle
        a                owl:Class ;
        rdfs:comment     "A collection of trace elements that were generated during a single plan execution." ;
        rdfs:label       "Execution Trace Bundle" ;
        rdfs:subClassOf  :Entity , <http://www.w3.org/ns/prov#Bundle> .

:EntityCollection  a     owl:Class ;
        rdfs:comment     "A collection of entities corresponding to ep-plan:MultiVariable. ep-pla:Entity contained in such collection corresponds to ep-plan:Variable that is linked to ep-plan:MultiVariable and defined in the subplan of a plan that the  ep-plan:MultiVariable is defined in.  Entity Collection is typically used or generated by  ep-plan:MultiActivity." ;
        rdfs:label       "Entity Collection" ;
        rdfs:subClassOf  :Entity , <http://www.w3.org/ns/prov#Collection> .

:includesVariable  a        owl:ObjectProperty ;
        rdfs:comment        "Inverse of ep-plan:isVariableOfPlan." ;
        rdfs:domain         :Plan ;
        rdfs:label          "includesVariable" ;
        rdfs:range          :Variable ;
        rdfs:subPropertyOf  :includesPlanElement ;
        owl:inverseOf       :isVariableOfPlan .

:isPartOf  a          owl:ObjectProperty ;
        rdfs:comment  "Inverse of ep-plan:hasPart." ;
        rdfs:domain   :Variable ;
        rdfs:label    "isPartOf" ;
        rdfs:range    :MultiVariable .

<http://purl.org/net/p-plan#correspondsToStep>
        a                       owl:ObjectProperty ;
        owl:equivalentProperty  :correspondsToStep .

<http://purl.org/net/p-plan#isVariableOfPlan>
        a                       owl:ObjectProperty ;
        owl:equivalentProperty  :isVariableOfPlan .

:Agent  a                owl:Class ;
        rdfs:comment     "A human or computational agent that can be responsible for the execution of an activity corresponding to a planned step or the overall plan execution." ;
        rdfs:label       "Agent" ;
        rdfs:subClassOf  <http://www.w3.org/ns/prov#Agent> .

:hasTraceElement  a    owl:ObjectProperty ;
        rdfs:comment   "ep-plan:hasTraceElement links ep-plan:ExecutionTraceBundle to execution trace elements ( i.e. ep-plan:Agent,ep-plan:Activity or ep-plan:Entity)." ;
        rdfs:domain    :ExecutionTraceBundle ;
        rdfs:label     "hasTraceElement" ;
        rdfs:range     [ a            owl:Class ;
                         owl:unionOf  ( :Activity :Agent :Communication :Entity )
                       ] ;
        owl:inverseOf  :isElementOfTrace .

<http://purl.org/net/p-plan#isSubPlanOfPlan>
        a                       owl:ObjectProperty ;
        owl:equivalentProperty  :isSubPlanOfPlan .

:satisfiedBy  a             owl:ObjectProperty ;
        rdfs:comment        "Inverse of ep-plan:satisfied." ;
        rdfs:domain         :Constraint ;
        rdfs:label          "satisfiedBy" ;
        rdfs:range          [ a            owl:Class ;
                              owl:unionOf  ( :Activity :Communication )
                            ] ;
        rdfs:subPropertyOf  :evaluatedFor .

:isQualifiedEvaluationOf
        a              owl:ObjectProperty ;
        rdfs:comment   "Inverse of ep-plan:qualifiedEvaluation." ;
        rdfs:domain    :ConstraintEvaluation ;
        rdfs:label     "isQualifiedEvaluationOf" ;
        rdfs:range     :Constraint ;
        owl:inverseOf  :qualifiedEvaluation .

:Rationale  a         owl:Class ;
        rdfs:comment  "A generic concept describing a specific justification of a plan or an individual plan element. For example, a  data anonymisation step might be included in the plan to protect personal information as a result of a data protection policy associated with a plan (i.e. the reason for including such a step in a plan). Similarly, a data protection policy associated with a plan can be linked to a description of the General Data Protection Regulation (GDPR) which provides the reason why such policy was implemented." ;
        rdfs:label    "Rationale" .

:correspondsToStep  a  owl:FunctionalProperty , owl:ObjectProperty ;
        rdfs:comment   "ep-plan:correspondsToStep links a ep-plan:Activity to its planned ep-plan:Step" ;
        rdfs:domain    :Activity ;
        rdfs:label     "correspondsToStep" ;
        rdfs:range     :Step ;
        owl:inverseOf  :hasCorrespondingActivity .

:isInputVariableOf  a  owl:ObjectProperty ;
        rdfs:comment  "Inverse of ep-plan:hasInputVariable" ;
        rdfs:domain   :Variable ;
        rdfs:label    "isInputVariableOf" ;
        rdfs:range    :Step .

:isPolicyOfPlan  a          owl:ObjectProperty ;
        rdfs:comment        "ep-plan:isPolicyOfPlan binds a ep-plan:Policy to the ep-plan:Plan it corresponds to." ;
        rdfs:domain         :Policy ;
        rdfs:label          "isPolicyOfPlan" ;
        rdfs:range          :Plan ;
        rdfs:subPropertyOf  :isElementOfPlan .

:hasConstraint  a     owl:ObjectProperty ;
        rdfs:comment  "ep-plan:hasConstraint links ep-plan:Step, ep-plan:CommunicationSpec or ep-plan:Plan to a ep-plan:Constraint. Please note, that we do not stipulate that this constriant must be strictly satisfied before the plan can continue. This is to enable inclusions of constraints defined by policies which can be violated under certain conditions and to also enable capture of incorrect behaviour of a system (e.g. a plan ignored a constriant)." ;
        rdfs:domain   [ a            owl:Class ;
                        owl:unionOf  ( :CommunicationSpecification :Plan :Step )
                      ] ;
        rdfs:label    "hasConstraint" ;
        rdfs:range    :Constraint .

<http://purl.org/net/p-plan#isStepOfPlan>
        a                       owl:ObjectProperty ;
        owl:equivalentProperty  :isStepOfPlan .

:Activity  a             owl:Class ;
        rdfs:comment     "An element of an execution trace representing an individual process/activity corresponding to a  step of a plan and which used and/or generated  trace entities." ;
        rdfs:label       "Activity" ;
        rdfs:subClassOf  <http://www.w3.org/ns/prov#Activity> .

:Communication  a        owl:Class ;
        rdfs:comment     "ep-plan:Communication is a type of prov:Communication representing a transfer of an entity between two activities according to some planned specification expressed as ep-plan:CommunicationSpecification." ;
        rdfs:label       "Communication" ;
        rdfs:subClassOf  <http://www.w3.org/ns/prov#Communication> .

<http://purl.org/net/p-plan#isDecomposedAsPlan>
        a                       owl:ObjectProperty ;
        owl:equivalentProperty  :isDecomposedAsPlan .

:includesSubPlan  a         owl:TransitiveProperty , owl:ObjectProperty ;
        rdfs:comment        "Inverse of ep-plan:isSubPlanOf." ;
        rdfs:domain         :Plan ;
        rdfs:label          "includesSubPlan" ;
        rdfs:range          :Plan ;
        rdfs:subPropertyOf  :includesPlanElement ;
        owl:inverseOf       :isSubPlanOfPlan .

:includesPlanElement  a  owl:ObjectProperty ;
        rdfs:comment   "Inverse of ep-plan:isElementOfPlan." ;
        rdfs:domain    :Plan ;
        rdfs:label     "includesPlanElement" ;
        rdfs:range     [ a            owl:Class ;
                         owl:unionOf  ( :CommunicationSpecification :Constraint :Objective :Plan :ResponsibleAgent :Step :Variable )
                       ] ;
        owl:inverseOf  :isElementOfPlan .
